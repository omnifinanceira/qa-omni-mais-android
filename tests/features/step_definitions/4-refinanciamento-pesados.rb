Quando('eu clico em Adicionar ficha refinanciamento pesado') do
  @novaficha = NovaFicha.new
  @novaficha.clicar_nova_ficha
end

Quando('seleciono o tipo de operacao refinanciamento para a ficha pesado') do
  @novaficha.selecionar_operacao_refinanciamento
end

Quando('seleciono o produto refinanciamento pesado') do
  @novaficha.selecionar_produto('PESADOS')
end

Quando('seleciono o solicitante lojista para a ficha refinanciamento pesado') do
  @novaficha.solicitante("LOJISTA")
end

Quando('seleciono a loja refinanciamento pesado') do
  @novaficha.selecionar_loja("LUSA VEÍCULOS")
end

Quando('seleciono o vendedor refinanciamento pesado') do
  @novaficha.selecionar_vendedor
end

Quando('preencho o {string} do cliente refinanciamento pesado') do |cpf|
  @novaficha.preencher_cpf(cpf)
end

Quando('preencho {string} do cliente refinanciamento pesado') do |datanascimento|
  @novaficha.preencher_data_nascimento(datanascimento)
end

Quando('preencho o numero de celular do cliente refinanciamento pesado') do
  @novaficha.preencher_celular
end

Quando('preencho a renda do cliente refinanciamento pesado') do
  @novaficha.preencher_renda_cliente
end

Quando('seleciono o comprovante de renda refinanciamento pesado') do
  @novaficha.selecionar_comprovante_renda
end

Quando('informo conjuge refinanciamento pesado') do
  @novaficha.adicionar_conjuge(false)
end

Quando('informo {string} caminhao cliente refinanciamento pesado') do |primeiro|
  primeiro == "true" ? primeiro = true : primeiro = false
  @novaficha.primeiro_caminhao_cliente(primeiro)
end

Quando('preencho {int} e informacoes do caminhao proprio refinanciamento pesado') do |quantidadecaminhao|
  @novaficha.preencher_caminhao_em_nome_cliente(quantidadecaminhao)
end

Quando('insiro dados do veiculo {string} refinanciamento pesado') do |placa|
  @novaficha.preencher_dados_veiculo(placa)
end

Quando('carrego o resultado parcial refinanciamento pesado') do
  @novaficha.aguardar_carregamento_resultado_parcial
end

Quando('ajusto o valor financiado e quantidade de parcelas refinanciamento pesado') do
  binding.pry
end

Quando('insiro mais informacoes do cliente refinanciamento pesado') do

end

Quando('passo por informacoes dos caminhoes do proprio cliente refinanciamento pesado') do

end

Quando('insiro endereco do cliente refinanciamento pesado') do

end

Quando('preencho observacoes refinanciamento pesado') do

end

Entao('valido se a ficha foi criada e enviada para analise refinanciamento pesado') do

end

Entao('validar os dados da proposta refinanciamento pesado') do

end