Quando('eu clico em Adicionar ficha financiamento leve') do
    @novaficha = NovaFicha.new
    @novaficha.clicar_nova_ficha
end

Quando('seleciono o tipo de operacao financiamento para a ficha leve') do
    @novaficha.selecionar_operacao_financiamento
end

Quando('seleciono o produto financiamento leve') do
    @novaficha.selecionar_produto("AUTOMÓVEIS")
end

Quando('seleciono o solicitante lojista para a ficha financiamento leve') do
    @novaficha.solicitante("LOJISTA")
end

Quando('seleciono a loja financiamento leve') do
    @novaficha.selecionar_loja("LUSA VEÍCULOS")
end

Quando('seleciono o vendedor financiamento leve') do
    @novaficha.selecionar_vendedor
end

Quando('preencho o {string} do cliente financiamento leve') do |cpf|
    @novaficha.preencher_cpf(cpf)
end

Quando('preencho {string} do cliente financiamento leve') do |datanascimento|
    @novaficha.preencher_data_nascimento(datanascimento)
end

Quando('preencho o numero de celular do cliente financiamento leve') do
    @novaficha.preencher_celular
end

Quando('preencho a renda do cliente financiamento leve') do
    @novaficha.preencher_renda_cliente
end

Quando('insiro dados do veiculo {string} financiamento leve') do |placa|
    @novaficha.preencher_dados_veiculo(placa)
end

Quando('carrego o resultado parcial financiamento leve') do
    @novaficha.aguardar_carregamento_resultado_parcial
end

Quando('seleciono o valor do veiculo e quantidade de parcelas') do
    @novaficha.selecionar_valor_financiamento
end