Quando('eu preencher o campo {string}') do |login|
  @login = Login.new
  @login.preencher_login(login)
end

Quando('preencho o campo {string}') do |senha|
  @login.preencher_senha(senha)
end

Quando('clico no botao Entrar') do
  @login.clicar_entrar
end

Entao('eu espero visualizar a home do OmniMais') do
  wait_true {expect(find_element(:id, "act_home_swipe_refresh").displayed?).to be true }
end