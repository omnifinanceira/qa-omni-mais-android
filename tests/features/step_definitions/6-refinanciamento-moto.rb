Quando('eu clico em Adicionar ficha refinanciamento moto') do
    @novaficha = NovaFicha.new
    @novaficha.clicar_nova_ficha
end

Quando('seleciono o tipo de operacao refinanciamento para a ficha moto') do
    @novaficha.selecionar_operacao_refinanciamento
end

Quando('seleciono o produto refinanciamento moto') do
    @novaficha.selecionar_produto("MOTOCICLETAS")
end

Quando('seleciono o solicitante lojista para a ficha refinanciamento moto') do
    @novaficha.solicitante("LOJISTA")
end

Quando('seleciono a loja refinanciamento moto') do
    @novaficha.selecionar_loja("LUSA VEÍCULOS")
end

Quando('seleciono o vendedor refinanciamento moto') do
    @novaficha.selecionar_vendedor
end

Quando('preencho o {string} do cliente refinanciamento moto') do |cpf|
    @novaficha.preencher_cpf(cpf)
end

Quando('preencho {string} do cliente refinanciamento moto') do |datanascimento|
    @novaficha.preencher_data_nascimento(datanascimento)
end

Quando('preencho o numero de celular do cliente refinanciamento moto') do
    @novaficha.preencher_celular
end

Quando('preencho a renda do cliente refinanciamento moto') do
    @novaficha.preencher_renda_cliente
end

Quando('insiro dados do veiculo {string} refinanciamento moto') do |placa|
    @novaficha.preencher_dados_veiculo(placa)
end

Quando('carrego o resultado parcial refinanciamento moto') do
    @novaficha.aguardar_carregamento_resultado_parcial
end