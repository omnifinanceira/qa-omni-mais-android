#language:pt
@5 @refinanciamento-leves
Funcionalidade: Criar nova proposta refinanciamento leves

Contexto:
    Dado que eu esteja logado como lojista

Cenario: Criar nova proposta refinanciamento leves
Quando eu clico em Adicionar ficha refinanciamento leve
E seleciono o tipo de operacao refinanciamento para a ficha leve
E seleciono o produto refinanciamento leve
E seleciono o solicitante lojista para a ficha refinanciamento leve
E seleciono a loja refinanciamento leve
E seleciono o vendedor refinanciamento leve
E preencho o <cpf> do cliente refinanciamento leve
E preencho <datanascimento> do cliente refinanciamento leve
E preencho o numero de celular do cliente refinanciamento leve
E preencho a renda do cliente refinanciamento leve
E insiro dados do veiculo <placa> refinanciamento leve
E carrego o resultado parcial refinanciamento leve

Exemplos:
    |cpf          |datanascimento  |placa    |
    |"04333332869"|"06/08/1962"    |"FHC9383"|