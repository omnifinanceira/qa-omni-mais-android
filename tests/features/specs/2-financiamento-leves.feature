#language:pt
@2 @financiamento-leves
Funcionalidade: Criar nova proposta financiamento leves

Contexto:
    Dado que eu esteja logado como lojista

Cenario: Criar nova proposta financiamento leves
Quando eu clico em Adicionar ficha financiamento leve
E seleciono o tipo de operacao financiamento para a ficha leve
E seleciono o produto financiamento leve
E seleciono o solicitante lojista para a ficha financiamento leve
E seleciono a loja financiamento leve
E seleciono o vendedor financiamento leve
E preencho o <cpf> do cliente financiamento leve
E preencho <datanascimento> do cliente financiamento leve
E preencho o numero de celular do cliente financiamento leve
E preencho a renda do cliente financiamento leve
E insiro dados do veiculo <placa> financiamento leve
E carrego o resultado parcial financiamento leve
E seleciono o valor do veiculo e quantidade de parcelas

Exemplos:
    |cpf          |datanascimento  |placa    |
    |"04333332869"|"06/08/1962"    |"FHC9383"|