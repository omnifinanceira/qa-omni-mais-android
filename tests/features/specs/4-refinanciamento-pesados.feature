#language:pt
@4 @refinanciamento-pesados
Funcionalidade: Criar nova proposta refinanciamento pesados

Contexto:
    Dado que eu esteja logado como lojista

Cenario: Criar nova proposta refinanciamento pesados
Quando eu clico em Adicionar ficha refinanciamento pesado
E seleciono o tipo de operacao refinanciamento para a ficha pesado
E seleciono o produto refinanciamento pesado
E seleciono o solicitante lojista para a ficha refinanciamento pesado
E seleciono a loja refinanciamento pesado
E seleciono o vendedor refinanciamento pesado
E preencho o <cpf> do cliente refinanciamento pesado
E preencho <datanascimento> do cliente refinanciamento pesado
E preencho o numero de celular do cliente refinanciamento pesado
E preencho a renda do cliente refinanciamento pesado
E seleciono o comprovante de renda refinanciamento pesado
E informo conjuge refinanciamento pesado
E informo <primeiro> caminhao cliente refinanciamento pesado
E preencho <quantidadecaminhao> e informacoes do caminhao proprio refinanciamento pesado
E insiro dados do veiculo <placa> refinanciamento pesado
E carrego o resultado parcial refinanciamento pesado
E ajusto o valor financiado e quantidade de parcelas refinanciamento pesado
E insiro mais informacoes do cliente refinanciamento pesado
E passo por informacoes dos caminhoes do proprio cliente refinanciamento pesado
E insiro endereco do cliente refinanciamento pesado
E preencho observacoes refinanciamento pesado
Entao valido se a ficha foi criada e enviada para analise refinanciamento pesado
E validar os dados da proposta refinanciamento pesado

Exemplos:
    |cpf          |datanascimento  |primeiro|quantidadecaminhao|placa    |
    |"04333332869"|"06/08/1962"    |"true"  |0                 |"DTB9C03"|
    # |"00585296839"|"11/12/1959"    |"false" |1                 |"MHD0112"|
    # |"52171523891"|"03/06/1951"    |"false" |2                 |"DWQ5170"|
    # |"42053615800"|"07/09/1994"|0|"APC0306"|
    # |"16601794830"|"06/07/1971"|0|"LKK8H06"|
    # |"13982384818"|"05/01/1974"|0|"AOM6D94"|
    # |"25657215877"|"19/03/1976"|0|"NGS8G11"|
    # |"12818545650"|"17/03/1995"|0|"EAC3B04"|
    # |"26724399823"|"12/08/1978"|0|"ACT9922"|
    # |"09709130870"|"29/10/1968"|0|"KYH0793"|